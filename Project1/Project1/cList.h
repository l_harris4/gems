#ifndef _cList_HG_
#define _cList_HG_
#include "cListItem.h"
#include "cPerson.h"


class cList {
public:
	cList();
	~cList();
	cListItem* first;
	cListItem* last;
	int size = 0;
	sPerson& operator[](unsigned int idx);
	void InsertAt(int index, sPerson person);
	void Sort(bool(*function) (const sPerson &first, const sPerson&second));
	void InsertUsingPredicate(sPerson person, bool(*function) (const sPerson &first, const sPerson&second));
	bool Empty();
	int Size();
};
#endif // !_cList_HG_

