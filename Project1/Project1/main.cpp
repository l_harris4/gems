#include <iostream>
#include "cLukesProjectClass.h"
using namespace std;
const int printOutAmount = 5;

void printTestVector(std::vector<sPerson>& testVector)
{
	for (int i = 0; i < printOutAmount; ++i)
	{
		cout << "Name: " << testVector[i].first << ", " << testVector[i].last <<
			" Health: " << testVector[i].health << " ID: " << testVector[i].uniqueID <<
			" Location: " << testVector[i].location.x << "," << testVector[i].location.y << "," << testVector[i].location.z << endl;
	}
}

int main()
{
	cLukesProjectClass* test = new cLukesProjectClass();
	//this is where you change which class is being tested
	test->setContainerType(eContainerType::CUSTOM_DIY_MAP);

	test->LoadDataFilesIntoContainer("USCen/dist.female.first.txt", "USCen/dist.male.first.txt", "USCen/US_LastNames.txt");
	test->GenerateRandomPeople(10000);

	cout << "Lukes testing of project class" << endl;
	std::vector<sPerson> testVector;
	sPerson testPerson;

	////now test and print all of the sorts
	testVector.clear();
	test->SortPeople(testVector, iPersonMotron::eSortType::ASC_BY_HEALTH);
	cout << endl << "Asc by health" << endl;
	printTestVector(testVector);

	testVector.clear();
	test->SortPeople(testVector, iPersonMotron::eSortType::DESC_BY_HEALTH);
	cout << endl << "Desc by health" << endl;
	printTestVector(testVector);

	testVector.clear();
	test->SortPeople(testVector, iPersonMotron::eSortType::ASC_BY_ID);
	cout << endl << "Asc by id" << endl;
	printTestVector(testVector);

	testVector.clear();
	test->SortPeople(testVector, iPersonMotron::eSortType::DESC_BY_ID);
	cout << endl << "Desc by id" << endl;
	printTestVector(testVector);

	testVector.clear();
	test->SortPeople(testVector, iPersonMotron::eSortType::ASC_FIRST_THEN_LAST);
	cout << endl << "Asc first then last" << endl;
	printTestVector(testVector);

	testVector.clear();
	test->SortPeople(testVector, iPersonMotron::eSortType::DESC_FIRST_THEN_LAST);
	cout << endl << "desc first then last" << endl;
	printTestVector(testVector);

	testVector.clear();
	test->SortPeople(testVector, iPersonMotron::eSortType::ASC_LAST_THEN_FIRST);
	cout << endl << "asc last then first" << endl;
	printTestVector(testVector);

	testVector.clear();
	test->SortPeople(testVector, iPersonMotron::eSortType::DESC_LAST_THEN_FIRST);
	cout << endl << "desc last then first" << endl;
	printTestVector(testVector);

	//end of sorting section


	//try to find a person by name with a blank person, should return everyone
	test->FindPeopleByName(testVector, testPerson);
	if (testVector.size() == 0)
		cout << "Failure, should have found a bunch of people" << endl;
	testPerson.first = "test";
	testPerson.last = "testy";
	testPerson.uniqueID = 9999;

	//printTestVector(testVector);

	//try to find a person, shouldnt find the person
	testVector.clear();
	test->FindPeopleByName(testVector,testPerson);
	if (testVector.size() > 0)
		cout << "Failure, found a person when shouldnt have" << endl;

	//add on person to for sure find
	test->addPerson(testPerson);
	testVector.clear();
	test->FindPeopleByName(testVector, testPerson);
	if (testVector.size() != 1)
		cout << "Failure, didn't find a person when should have" << endl;


	std::vector<sPerson> compareVector;
	compareVector.push_back(testPerson);
	testPerson.first = "test1";
	testPerson.uniqueID = 9998;
	compareVector.push_back(testPerson);

	testVector.clear();
	test->FindPeopleByName(testVector, compareVector);
	if (testVector.size() != 1)
		cout << "Failure, didn't find a person when should have" << endl;
	test->addPerson(testPerson);

	testVector.clear();
	test->FindPeopleByName(testVector, compareVector);
	//custom map failing on this one
	if (testVector.size() != 2)
		cout << "Failure, didn't find a person when should have" << endl;


	test->FindPersonByID(testPerson, 1);
	if (testPerson.first == "test1" )
		cout << "Failure, find person by id didn't work" << endl;

	testVector.clear();

	sPoint origin;
	origin.x = 0;
	origin.y = 0;
	origin.z = 0;
	test->FindPeople(testVector, origin, 500);
	if (testVector.size() == 0)
		cout << "Failure, really should have found at least one" << endl;

	testVector.clear();
	test->FindPeople(testVector, origin, 1000,10);
	if (testVector.size() > 10)
		cout << "Failure, should have only found 10" << endl;

	testVector.clear();
	test->FindPeople(testVector, origin, 2000);
	if (testVector.size() < test->numPeopleGenerated)
		cout << "Failure, should have found all of them" << endl;

	testVector.clear();
	test->FindPeople(testVector, 0.0f,100.0f);
	if (testVector.size() < test->numPeopleGenerated)
		cout << "Failure, should have returned all of them" << endl;

	testVector.clear();
	test->FindPeople(testVector, 0.0f, 100.0f, 10);
	if (testVector.size() > 10)
		cout << "Failure, should have only found 10" << endl;

	
	testVector.clear();
	test->FindPeople(testVector, 0.0f, 50.0f);
	if (testVector.size() == test->numPeopleGenerated)
		cout << "Failure, probably shouldnt have returned all of them" << endl;


	testVector.clear();
	test->FindPeople(testVector, origin, 2000, 0.0f, 100.0f);
	if (testVector.size() < test->numPeopleGenerated)
		cout << "Failure, should have found all of them" << endl;

	testVector.clear();
	test->FindPeople(testVector, origin, 500, 0.0f, 50.0f);
	if (testVector.size() >= test->numPeopleGenerated)
		cout << "Failure, shouldnt have had any of them" << endl;



	system("pause");

	return 0;
}