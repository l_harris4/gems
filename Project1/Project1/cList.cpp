#include "cList.h"

cList::cList()
{
	first = 0;
	last = 0;
}

cList::~cList()
{
	delete first;
}


sPerson & cList::operator[](unsigned int idx)
{
	//go through the list until the index is reached and return that value
	cListItem* returnValue = first;
	for (int i = 0; i < idx; ++i)
	{
		returnValue = returnValue->nextValue;
	}
	return returnValue->value;
}

void cList::InsertAt(int index, sPerson person)
{
	//if the list is empty
	if (first == 0)
	{
		first = new cListItem(person);
		size = 1;
	}
	else
	{ //if the list is not empty we have to find where to put the new list item
		cListItem* current = first;
		for (int i = 0; i < index; ++i)
		{
			if (current->nextValue == 0)
				break;
			current = current->nextValue;
		}
		sPerson holder = current->value;
		current->value = person;


		cListItem* newListItem = new cListItem(holder);
		newListItem->prevValue = current;
		newListItem->nextValue = current->nextValue;
		current->nextValue = newListItem;
		size++;
	}
}

void cList::Sort(bool(*function)(const sPerson &first, const sPerson &second))
{
	cList* copyList = new cList();
	cListItem* current = first;
	while (current != 0)
	{
		copyList->InsertUsingPredicate(current->value, function);
		current = current->nextValue;
	}

	//cListItem* holder = copyList->last->prevValue;
	//sPerson holderPerson = copyList->last->value;
	//delete copyList->last;
	//copyList->last = holder;


	//copyList->InsertUsingPredicate(holderPerson, function);



	delete first;
	*this = *copyList;
}

void cList::InsertUsingPredicate(sPerson person, bool(*function)(const sPerson &first, const sPerson &second))
{
	//if the list is empty
	if (first == 0)
	{
		first = new cListItem(person);
		last = first;
		size = 1;
	}
	else
	{
		cListItem* current = first;
		while (current->nextValue != 0 && function(current->value,person))
		{
			current = current->nextValue;
		}

		if (function(current->value, person))
		{
			cListItem* newListItem = new cListItem(person);
			newListItem->prevValue = current;
			newListItem->nextValue = current->nextValue;
			current->nextValue = newListItem;

			if (current == last)
			{
				last = newListItem;
			}
		}
		else
		{
			sPerson holder = current->value;
			current->value = person;

			cListItem* newListItem = new cListItem(holder);
			newListItem->prevValue = current;
			newListItem->nextValue = current->nextValue;
			current->nextValue = newListItem;

			if (current == last)
			{
				last = newListItem;
			}
		}
		size++;
	}
}

bool cList::Empty()
{
	if (first != NULL)
		return false;
	else
		return true;
}

int cList::Size()
{
	return size;
}
