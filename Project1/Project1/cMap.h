#ifndef cMAP_HG_
#define cMAP_HG_
#include "cList.h"
#include "cVector.h"

class cMap
{
public:
	cMap();
	~cMap();

	static const unsigned int INITALARRAYSIZE = 10000;
	cVector* people;
	sPerson* operator[](unsigned int idx);
	sPerson* operator[](std::string key);

	unsigned int size = 0;

	bool Insert(unsigned int idx, sPerson person);
	bool Insert(std::string idx, sPerson person);
	bool Insert(std::string firstIdentifier, std::string secondIdentifier, sPerson person);
	//bool overwrite

	unsigned int genHash(std::string key);
	unsigned int genHash(std::string key, int multiplier);

	unsigned int Size();

};
#endif // !cMAP_HG_

