#ifndef _cVector_HG_
#define _cVector_HG_
#include "cPerson.h"

class cVector
{
public:

	//typedef T * iterator;
	sPerson* contents;// [10];

	cVector(unsigned int size = 10);
	//cVector(const cVector<T> & v);
	~cVector();
	unsigned int m_Size;
	unsigned int m_Consumed;


	unsigned int Capacity();
	unsigned int Size();
	void PushBack(const sPerson & value);

	sPerson& operator[](unsigned int idx) {
		return contents[idx]; 
	}
	const sPerson& operator[](unsigned int idx) const {
		return contents[idx]; 
	}

	void Sort(int left, int right, bool (*function) (const sPerson &first, const sPerson&second));


	//void PopBack();
	//bool Empty();

};

//#include "cVector.cpp"
#endif // !_cVector_HG_




