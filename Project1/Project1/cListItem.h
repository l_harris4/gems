#ifndef _cListItem_HG_
#define _cListItem_HG_
#include "cPerson.h"

class cListItem {
public:
	cListItem(sPerson value);
	~cListItem();
	sPerson value;
	cListItem* nextValue;
	cListItem* prevValue;
};
#endif // !_cListItem_HG_


