#include "cVector.h"


unsigned int cVector::Capacity()
{
	return this->m_Size - this->m_Consumed;
}

cVector::cVector(unsigned int size)
{
	this->m_Size = size;
	this->m_Consumed = 0;
	this->contents = new sPerson[size];
}

cVector::~cVector()
{
}

unsigned int cVector::Size()
{
	return this->m_Consumed;
}


void cVector::PushBack(const sPerson & value)
{
	//if the vector is full
	if (this->m_Consumed == this->m_Size)
	{
		//we have to recreate the interal array
		//and copy over all the contents
		sPerson* newContents = new sPerson[this->m_Size * 2];
		for (int i = 0; i < this->m_Size; ++i)
		{
			newContents[i] = contents[i];
		}
		newContents[this->m_Consumed] = value;

		//delete from class
		delete[] this->contents;

		contents = newContents;
		this->m_Size = this->m_Size * 2;
		this->m_Consumed++;

	}
	else//if the vector is not full
	{
		//just insert the value on the back
		contents[this->m_Consumed] = value;
		this->m_Consumed++;

	}
}


//this is the quicksort algorithm taken from wikipedia
void cVector::Sort(int left, int right, bool(*predicate) (const sPerson &first, const sPerson&second))
{

	int i = left, j = right;
	sPerson tmp;
	sPerson pivot = contents[(left + right) / 2];

	/* partition */
	while (i <= j) {
		while (predicate(contents[i],pivot))
			i++;

		while (predicate(pivot, contents[j]))
			j--;

		if (i <= j) {
			tmp = contents[i];
			contents[i] = contents[j];
			contents[j] = tmp;
			i++;
			j--;
		}
	};

	/* recursion */
	if (left < j)
		Sort(left, j, predicate);

	if (i < right)
		Sort(i, right, predicate);
}
