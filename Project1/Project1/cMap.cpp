#include "cMap.h"
#include "cList.h"

cMap::cMap()
{
	this->people = new cVector[this->INITALARRAYSIZE];
}

cMap::~cMap()
{
}

bool cMap::Insert(unsigned int idx, sPerson person)
{
	if (idx >= INITALARRAYSIZE -1)
	{
		idx = idx % INITALARRAYSIZE;
	}
	people[idx].PushBack(person);
	size++;
	return true;
}

bool cMap::Insert(std::string key, sPerson person)
{
	Insert(genHash(key), person);
	return true;
}

bool cMap::Insert(std::string firstIdentifier, std::string secondIdentifier, sPerson person)
{
	Insert(genHash(firstIdentifier,2) + genHash(secondIdentifier), person);
	return true;
}

//need one for insert and one for overwrite and one for just return
sPerson* cMap::operator[](unsigned int idx)
{
	if (people[idx].Size() == 0)
	{
		sPerson person;
		people[idx].PushBack(person);
		return &people[idx][0];
		size++;
	}
	else
	{
		return &people[idx][0];
	}
}

sPerson* cMap::operator[](std::string key)
{
	return this->operator[](genHash(key));
}





unsigned int cMap::genHash(std::string key)
{
	// Take ASCII value for each letter and add them up!
	unsigned int theHashValue = 0;
	//int counterWeight = 10;

	for (int index = 0; index != key.size(); index++)
	{
		char curChar = key[index];
		theHashValue += (unsigned int)curChar;
		if (index == 0)
		{
			theHashValue *= 20;
		}
		//counterWeight -= 10;
	}

	// 2. divide hash by size of the array
	theHashValue = theHashValue % INITALARRAYSIZE;

	return theHashValue;
}

unsigned int cMap::genHash(std::string key, int multiplier)
{
	// Take ASCII value for each letter and add them up!
	unsigned int theHashValue = 0;

	for (int index = 0; index != key.size(); index++)
	{
		char curChar = key[index];
		theHashValue += 37* theHashValue + (unsigned int)curChar * multiplier;
	}

	// 2. divide hash by size of the array
	theHashValue = theHashValue % INITALARRAYSIZE;

	return theHashValue;
}

unsigned int cMap::Size()
{
	return size;
}
