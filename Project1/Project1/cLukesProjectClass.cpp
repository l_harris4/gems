#include "cLukesProjectClass.h"
#include <fstream>
#include "cPerson.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "windows.h"
#include <psapi.h>
#include <iostream>
#include <iomanip>

#pragma comment(lib, "psapi.lib")




int RandomInt(int min, int max)
{
	int r = rand() % max + min;
	return r;
} // end RandomInt()

float Distance(sPoint first, sPoint second)
{
	return sqrt(pow((first.x - second.x), 2) + pow((first.y - second.y), 2) + pow((first.z - second.z), 2));
}

bool NameMatches(sPerson* first, sPerson* second)
{
	if (first->first != "" && first->first != second->first)
	{
		return false;
	}
	if (first->last != "" && first->last != second->last)
	{
		return false;
	}

	return true;
}

bool CompareASCFIRSTTHENLAST(const sPerson & first, const sPerson &second)
{
	if (first.first < second.first)
		return true;
	if (first.first == second.first)
	{
		if (first.last < second.last)
			return true;
	}
	return false;
}

bool CompareDESCFIRSTTHENLAST(const sPerson &first, const sPerson &second)
{
	if (first.first > second.first)
		return true;
	if (first.first == second.first)
	{
		if (first.last > second.last)
			return true;
	}
	return false;
}

bool CompareASCLASTTHENFIRST(const sPerson &first, const sPerson &second)
{
	if (first.last < second.last)
		return true;
	if (first.last == second.last)
	{
		if (first.first < second.first)
			return true;
	}
	return false;
}

bool CompareDESCLASTTHENFIRST(const sPerson &first, const sPerson &second)
{
	if (first.last > second.last)
		return true;
	if (first.last == second.last)
	{
		if (first.first > second.first)
			return true;
	}
	return false;
}

bool CompareASCBYID(const sPerson &first, const sPerson &second)
{
	if (first.uniqueID < second.uniqueID)
		return true;
	return false;
}

bool CompareDESCBYID(const sPerson &first, const sPerson &second)
{
	if (first.uniqueID > second.uniqueID)
		return true;
	return false;
}

bool CompareASCBYHEALTH(const sPerson &first, const sPerson &second)
{
	if (first.health < second.health)
		return true;
	return false;
}

bool CompareDESCBYHEALTH(const sPerson &first, const sPerson &second)
{
	if (first.health > second.health)
		return true;
	return false;
}

void cLukesProjectClass::endCount()
{


	std::chrono::time_point<std::chrono::steady_clock> endTime = std::chrono::steady_clock::now();
	std::chrono::milliseconds elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);

	float end_memUsage = getCurrentMemUse(); // getCurrentValue();

	performanceData.elapsedCallTime_ms = (float)elapsed.count();
	if (start_memUsage < end_memUsage)
	{
		performanceData.memoryUsageBytes_min = start_memUsage;
		performanceData.memoryUsageBytes_max = end_memUsage;
	}
	else
	{
		performanceData.memoryUsageBytes_min = end_memUsage;
		performanceData.memoryUsageBytes_max = start_memUsage;
	}

	performanceData.memoryUsageBytes_avg = (end_memUsage + start_memUsage) / 2.0f;
}

cLukesProjectClass::~cLukesProjectClass()
{
	delete cVecMaleFirstNames;
	delete cVecFemaleFirstNames;
	delete cVecLastNames;

	delete cListMaleFirstNames;
	delete cListFemaleFirstNames;
	delete cListLastNames;

	delete cMapMaleFirstNames;
	delete cMapFemaleFirstNames;
	delete cMapLastNames;
}

bool cLukesProjectClass::LoadDataFilesIntoContainer(std::string firstNameFemaleFileName, std::string firstNameMaleFileName, std::string lastNameFileName)
{
	startTime = std::chrono::steady_clock::now();
	init(); // get the starting point memory data
	start_memUsage = getCurrentMemUse();



	std::fstream fileFemNames;
	fileFemNames.open(firstNameFemaleFileName);
	std::string line = "";
	int count = -1;
	while (std::getline(fileFemNames, line))
	{
		count++;
		switch (currentContainerType)
		{
		case eContainerType::STD_VECTOR:
			vecFemaleFirstNames.push_back(line.substr(0, line.find(' ')));
			break;
		case eContainerType::STD_LIST:
			listFemaleFirstNames.insert(listFemaleFirstNames.begin(), line.substr(0, line.find(' ')));
			break;
		case eContainerType::STD_MAP:
			mapFemaleFirstNames[count] = (line.substr(0, line.find(' ')));
			break;
		case eContainerType::CUSTOM_DIY_VECTOR:
		{
			sPerson tempP;
			tempP.first = (line.substr(0, line.find(' ')));
			cVecFemaleFirstNames->PushBack(tempP);
		}
		break;
		case eContainerType::CUSTOM_DIY_LIST:
		{
			sPerson tempP;
			tempP.first = (line.substr(0, line.find(' ')));
			cListFemaleFirstNames->InsertAt(0, tempP);
		}
		break;
		case eContainerType::CUSTOM_DIY_MAP:
		{
			sPerson tempP;
			tempP.first = (line.substr(0, line.find(' ')));
			cMapFemaleFirstNames->Insert(count, tempP);
		}
		break;
		}

	}
	fileFemNames.close();

	std::fstream fileMaleNames;
	fileMaleNames.open(firstNameMaleFileName);
	count = -1;
	while (std::getline(fileMaleNames, line))
	{
		count++;
		switch (currentContainerType)
		{
		case eContainerType::STD_VECTOR:
			vecMaleFirstNames.push_back(line.substr(0, line.find(' ')));
			break;
		case eContainerType::STD_LIST:
			listMaleFirstNames.insert(listMaleFirstNames.begin(), line.substr(0, line.find(' ')));
			break;
		case eContainerType::STD_MAP:
			mapMaleFirstNames[count] = (line.substr(0, line.find(' ')));
			break;
		case eContainerType::CUSTOM_DIY_VECTOR:
		{
			sPerson tempP;
			tempP.first = (line.substr(0, line.find(' ')));
			cVecMaleFirstNames->PushBack(tempP);
		}
		break;
		case eContainerType::CUSTOM_DIY_LIST:
		{
			sPerson tempP;
			tempP.first = (line.substr(0, line.find(' ')));
			cListMaleFirstNames->InsertAt(0, tempP);
		}
		break;
		case eContainerType::CUSTOM_DIY_MAP:
		{
			sPerson tempP;
			tempP.first = (line.substr(0, line.find(' ')));
			cMapMaleFirstNames->Insert(count, tempP);
		}
		break;
		}

	}
	fileMaleNames.close();

	std::fstream fileLastNames;
	fileLastNames.open(lastNameFileName);
	count = -1;
	while (std::getline(fileLastNames, line))
	{
		count++;
		switch (currentContainerType)
		{
		case eContainerType::STD_VECTOR:
			vecLastNames.push_back(line.substr(0, line.find(' ')));
			break;
		case eContainerType::STD_LIST:
			listLastNames.insert(listLastNames.begin(), line.substr(0, line.find(' ')));
			break;
		case eContainerType::STD_MAP:
			mapLastNames[count] = (line.substr(0, line.find(' ')));
			break;
		case eContainerType::CUSTOM_DIY_VECTOR:
		{
			sPerson tempP;
			tempP.last = (line.substr(0, line.find(' ')));
			cVecLastNames->PushBack(tempP);
		}
		break;
		case eContainerType::CUSTOM_DIY_LIST:
		{
			sPerson tempP;
			tempP.last = (line.substr(0, line.find(' ')));
			cListLastNames->InsertAt(0, tempP);
		}
		break;
		case eContainerType::CUSTOM_DIY_MAP:
		{
			sPerson tempP;
			tempP.last = (line.substr(0, line.find(' ')));
			cMapLastNames->Insert(count, tempP);
		}
		break;
		}
	}
	fileLastNames.close();

	//if (vecFemaleFirstNames.size() == 0 || vecMaleFirstNames.size() == 0 || vecLastNames.size() == 0)
	//	return false;

	//now generate random people and put them into the chosen container
	srand(time(NULL));
	

	endCount();

	return true;
}

bool cLukesProjectClass::GenerateRandomPeople(int numberOfPeople)
{
	numPeopleGenerated = numberOfPeople;
	startTime = std::chrono::steady_clock::now();
	start_memUsage = getCurrentMemUse();

	currentAddingID = 0;
	peopleVector.clear();
	peopleMap.clear();
	peopleList.clear();
	//clear custom containers as well

	for (int i = 0; i < numberOfPeople; ++i)
	{
		sPerson tempP;
		int femaleOrMale = RandomInt(0, 10);
		if (femaleOrMale > 5)
		{
			switch (currentContainerType)
			{
			case eContainerType::STD_VECTOR:
			{
				int randFirstNameIndex = RandomInt(0, vecMaleFirstNames.size() - 1);
				tempP.first = vecMaleFirstNames[randFirstNameIndex];
			}
			break;
			case eContainerType::STD_LIST:
			{
				int randFirstNameIndex = RandomInt(0, listMaleFirstNames.size() - 1);
				std::list<std::string>::iterator it = listMaleFirstNames.begin();
				std::advance(it, randFirstNameIndex);
				tempP.first = (*it);
			}
			break;
			case eContainerType::STD_MAP:
			{
				int randFirstNameIndex = RandomInt(0, mapMaleFirstNames.size() - 1);
				tempP.first = mapMaleFirstNames[randFirstNameIndex];
			}
			break;
			case eContainerType::CUSTOM_DIY_VECTOR:
			{
				int randFirstNameIndex = RandomInt(0, cVecMaleFirstNames->Size() - 1);
				tempP.first = (*cVecMaleFirstNames)[randFirstNameIndex].first;
			}
			break;
			case eContainerType::CUSTOM_DIY_LIST:
			{
				int randFirstNameIndex = RandomInt(0, cListMaleFirstNames->Size() - 1);
				tempP.first = (*cListMaleFirstNames)[randFirstNameIndex].first;
			}
			break;
			case eContainerType::CUSTOM_DIY_MAP:
			{
				//choose a random index
				int randFirstNameIndex = RandomInt(0, cMapMaleFirstNames->INITALARRAYSIZE);

				//start looking for a person at that random index
				for (int i = randFirstNameIndex; i < (*cMapMaleFirstNames).INITALARRAYSIZE; ++i)
				{
					//if we find a person, use their name and break
					//if we dont find a person simply move to the next
					for (int j = 0; j < (*cMapMaleFirstNames).people[i].Size(); ++j)
					{
						tempP.first = (*cMapMaleFirstNames).people[i][j].first;
						break;
					}
					if (tempP.first != "")
						break;
				}

				//if a person was not found in the first loop, search the rest of the map for a person
				if (tempP.first == "")
				{
					for (int i = 0; i < randFirstNameIndex; ++i)
					{
						for (int j = 0; j < (*cMapMaleFirstNames).people[i].Size(); ++j)
						{
							tempP.first = (*cMapMaleFirstNames).people[i][j].first;
							break;
						}
						if (tempP.first != "")
							break;
					}
				}
			}
			break;
			}

		}
		else
		{
			switch (currentContainerType)
			{
			case eContainerType::STD_VECTOR:
			{
				int randFirstNameIndex = RandomInt(0, vecFemaleFirstNames.size() - 1);
				tempP.first = vecFemaleFirstNames[randFirstNameIndex];
			}
			break;
			case eContainerType::STD_LIST:
			{
				int randFirstNameIndex = RandomInt(0, listFemaleFirstNames.size() - 1);
				std::list<std::string>::iterator it = listFemaleFirstNames.begin();
				std::advance(it, randFirstNameIndex);
				tempP.first = (*it);
			}
			break;
			case eContainerType::STD_MAP:
			{
				int randFirstNameIndex = RandomInt(0, mapFemaleFirstNames.size() - 1);
				tempP.first = mapFemaleFirstNames[randFirstNameIndex];
			}
			break;
			case eContainerType::CUSTOM_DIY_VECTOR:
			{
				int randFirstNameIndex = RandomInt(0, cVecFemaleFirstNames->Size() - 1);
				tempP.first = (*cVecFemaleFirstNames)[randFirstNameIndex].first;
			}
			break;
			case eContainerType::CUSTOM_DIY_LIST:
			{
				int randFirstNameIndex = RandomInt(0, cListFemaleFirstNames->Size() - 1);
				tempP.first = (*cListFemaleFirstNames)[randFirstNameIndex].first;
			}
			break;
			case eContainerType::CUSTOM_DIY_MAP:
			{
				//choose a random index
				int randFirstNameIndex = RandomInt(0, cMapFemaleFirstNames->INITALARRAYSIZE);

				//start looking for a person at that random index
				for (int i = randFirstNameIndex; i < (*cMapFemaleFirstNames).INITALARRAYSIZE; ++i)
				{
					//if we find a person, use their name and break
					//if we dont find a person simply move to the next
					for (int j = 0; j < (*cMapFemaleFirstNames).people[i].Size(); ++j)
					{
						tempP.first = (*cMapFemaleFirstNames).people[i][j].first;
						break;
					}
					if (tempP.first != "")
						break;
				}

				//if a person was not found in the first loop, search the rest of the map for a person
				if (tempP.first == "")
				{
					for (int i = 0; i < randFirstNameIndex; ++i)
					{
						for (int j = 0; j < (*cMapFemaleFirstNames).people[i].Size(); ++j)
						{
							tempP.first = (*cMapFemaleFirstNames).people[i][j].first;
							break;
						}
						if (tempP.first != "")
							break;
					}
				}
			}
				break;
			}

		}


		switch (currentContainerType)
		{
		case eContainerType::STD_VECTOR:
		{
			int randFirstNameIndex = RandomInt(0, vecLastNames.size() - 1);
			tempP.last = vecLastNames[randFirstNameIndex];
		}
		break;
		case eContainerType::STD_LIST:
		{
			int randFirstNameIndex = RandomInt(0, listLastNames.size() - 1);
			std::list<std::string>::iterator it = listLastNames.begin();
			std::advance(it, randFirstNameIndex);
			tempP.last = (*it);
		}
		break;
		case eContainerType::STD_MAP:
		{
			int randFirstNameIndex = RandomInt(0, mapLastNames.size() - 1);
			tempP.last = mapLastNames[randFirstNameIndex];
		}
		break;
		case eContainerType::CUSTOM_DIY_VECTOR:
		{
			int randFirstNameIndex = RandomInt(0, cVecLastNames->Size() - 1);
			tempP.last = (*cVecLastNames)[randFirstNameIndex].last;
		}
		break;
		case eContainerType::CUSTOM_DIY_LIST:
		{
			int randFirstNameIndex = RandomInt(0, cListLastNames->Size() - 1);
			tempP.last = (*cListLastNames)[randFirstNameIndex].last;
		}
		break;
		case eContainerType::CUSTOM_DIY_MAP:
			//choose a random index
			int randFirstNameIndex = RandomInt(0, cMapLastNames->INITALARRAYSIZE);

			//start looking for a person at that random index
			for (int i = randFirstNameIndex; i < (*cMapLastNames).INITALARRAYSIZE; ++i)
			{
				//if we find a person, use their name and break
				//if we dont find a person simply move to the next
				for (int j = 0; j < (*cMapLastNames).people[i].Size(); ++j)
				{
					tempP.last = (*cMapLastNames).people[i][j].last;
					break;
				}
				if (tempP.last != "")
					break;
			}

			//if a person was not found in the first loop, search the rest of the map for a person
			if (tempP.last == "")
			{
				for (int i = 0; i < randFirstNameIndex; ++i)
				{
					for (int j = 0; j < (*cMapLastNames).people[i].Size(); ++j)
					{
						tempP.last = (*cMapLastNames).people[i][j].last;
						break;
					}
					if (tempP.last != "")
						break;
				}
			}
			break;
		}


		tempP.uniqueID = currentAddingID;
		currentAddingID++;

		//give the person a random location as well
		tempP.location.x = RandomInt(0, 1000);
		tempP.location.y = RandomInt(0, 1000);
		tempP.location.z = RandomInt(0, 1000);

		//give the person a random health
		tempP.health = RandomInt(0, 100);


		switch (currentContainerType)
		{
		case eContainerType::STD_VECTOR:
			peopleVector.push_back(tempP);
			break;
		case eContainerType::STD_LIST:
			peopleList.insert(peopleList.begin(), tempP);
			break;
		case eContainerType::STD_MAP:
			peopleMap[tempP.uniqueID] = tempP;
			break;
		case eContainerType::CUSTOM_DIY_VECTOR:
			customVector.PushBack(tempP);
			break;
		case eContainerType::CUSTOM_DIY_LIST:
			customList.InsertAt(0, tempP);
			break;
		case eContainerType::CUSTOM_DIY_MAP:
			customMap.Insert(tempP.uniqueID, tempP);
			break;
		}

	}
	endCount();
	return true;
}

bool cLukesProjectClass::FindPeopleByName(std::vector<sPerson>& vecPeople, sPerson personToMatch, int maxNumberOfPeople)
{

	startTime = std::chrono::steady_clock::now();
	start_memUsage = getCurrentMemUse();
	switch (currentContainerType)
	{
	case eContainerType::STD_VECTOR:
		for (int i = 0; i < peopleVector.size(); ++i)
		{
			if (NameMatches(&personToMatch, &peopleVector[i]))
			{
				if (vecPeople.size() < maxNumberOfPeople)
					vecPeople.push_back(peopleVector[i]);
			}
		}
		break;
	case eContainerType::STD_LIST:
		for (std::list<sPerson>::iterator iterator = peopleList.begin(), end = peopleList.end(); iterator != end; ++iterator) {
			if (NameMatches(&personToMatch, &(*iterator)))
			{
				if (vecPeople.size() < maxNumberOfPeople)
					vecPeople.push_back((*iterator));
			}
		}
		break;
	case eContainerType::STD_MAP:
		for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {
			if (NameMatches(&personToMatch, &(*iterator).second))
			{
				if (vecPeople.size() < maxNumberOfPeople)
					vecPeople.push_back((*iterator).second);
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_VECTOR:
		for (int i = 0; i < customVector.Size(); ++i)
		{
			if (NameMatches(&personToMatch, &customVector[i]))
			{
				if (vecPeople.size() < maxNumberOfPeople)
					vecPeople.push_back(customVector[i]);
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_LIST:
	{
		cListItem* current = customList.first;
		while (current != 0)
		{
			if ((NameMatches(&personToMatch, &current->value)))
			{
				if (vecPeople.size() < maxNumberOfPeople)
					vecPeople.push_back(current->value);
			}
			current = current->nextValue;
		}
	}
	break;
	case eContainerType::CUSTOM_DIY_MAP:
	{
		int count = 0;
		for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
		{
			/*if (customMap[i].first != "")
				std::cout << std::endl << customMap[i].first;*/

			for (int j = 0; j < customMap.people[i].Size(); ++j)
			{
				if (NameMatches(&personToMatch, &customMap.people[i][j]))
				{
					if (vecPeople.size() < maxNumberOfPeople)
						vecPeople.push_back(customVector[j]);
				}
			}

		}
		int n = count;
	}
	break;
	}
	endCount();
	return true;
}

bool cLukesProjectClass::FindPeopleByName(std::vector<sPerson>& vecPeople, std::vector<sPerson>& vecPeopleToMatch, int maxNumberOfPeople)
{

	startTime = std::chrono::steady_clock::now();
	start_memUsage = getCurrentMemUse();

	switch (currentContainerType)
	{
	case eContainerType::STD_VECTOR:
		for (int j = 0; j < vecPeopleToMatch.size(); ++j)
		{
			for (int i = 0; i < peopleVector.size(); ++i)
			{
				if (NameMatches(&vecPeopleToMatch[j], &peopleVector[i]))
				{
					if (vecPeople.size() < maxNumberOfPeople)
						vecPeople.push_back(peopleVector[i]);
				}
			}
		}
		break;
	case eContainerType::STD_LIST:
		for (int j = 0; j < vecPeopleToMatch.size(); ++j)
		{
			for (std::list<sPerson>::iterator iterator = peopleList.begin(), end = peopleList.end(); iterator != end; ++iterator) {
				if (NameMatches(&vecPeopleToMatch[j], &(*iterator)))
				{
					if (vecPeople.size() < maxNumberOfPeople)
						vecPeople.push_back((*iterator));
				}
			}
		}
		break;
	case eContainerType::STD_MAP:
		for (int j = 0; j < vecPeopleToMatch.size(); ++j)
		{
			for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {
				if (NameMatches(&vecPeopleToMatch[j], &(*iterator).second))
				{
					if (vecPeople.size() < maxNumberOfPeople)
						vecPeople.push_back((*iterator).second);
				}
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_VECTOR:
		for (int j = 0; j < vecPeopleToMatch.size(); ++j)
		{
			for (int i = 0; i < customVector.Size(); ++i)
			{
				if (NameMatches(&vecPeopleToMatch[j], &customVector[i]))
				{
					if (vecPeople.size() < maxNumberOfPeople)
						vecPeople.push_back(customVector[i]);
				}
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_LIST:
		for (int j = 0; j < vecPeopleToMatch.size(); ++j)
		{
			cListItem* current = customList.first;
			while (current != 0)
			{
				if ((NameMatches(&vecPeopleToMatch[j], &current->value)))
				{
					if (vecPeople.size() < maxNumberOfPeople)
						vecPeople.push_back(current->value);
				}
				current = current->nextValue;
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_MAP:
		for (int j = 0; j < vecPeopleToMatch.size(); ++j)
		{
			for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < customMap.people[i].Size(); ++k)
				{
					if (NameMatches(&vecPeopleToMatch[j], &customMap.people[i][k]))
					{
						if (vecPeople.size() < maxNumberOfPeople)
							vecPeople.push_back(customVector[k]);
					}
				}
			}
		}
		break;
	}
	endCount();
	return true;
}

bool cLukesProjectClass::FindPersonByID(sPerson & person, unsigned long long uniqueID)
{

	startTime = std::chrono::steady_clock::now();
	start_memUsage = getCurrentMemUse();

	switch (currentContainerType)
	{
	case eContainerType::STD_VECTOR:
		for (int i = 0; i < peopleVector.size(); ++i)
		{
			if (peopleVector[i].uniqueID == uniqueID)
			{
				person = peopleVector[i];
				return true;
			}
		}
		break;
	case eContainerType::STD_LIST:
		for (std::list<sPerson>::iterator iterator = peopleList.begin(), end = peopleList.end(); iterator != end; ++iterator) {
			if ((*iterator).uniqueID == uniqueID)
			{
				person = (*iterator);
				return true;
			}
		}
		break;
	case eContainerType::STD_MAP:
		for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {
			if ((*iterator).second.uniqueID == uniqueID)
			{
				person = (*iterator).second;
				return true;
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_VECTOR:
		for (int i = 0; i < customVector.Size(); ++i)
		{
			if (customVector[i].uniqueID == uniqueID)
			{
				person = customVector[i];
				return true;
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_LIST:
	{
		cListItem* current = customList.first;
		while (current != 0)
		{
			if (current->value.uniqueID == uniqueID)
			{
				person = current->value;
				return true;
			}
			current = current->nextValue;
		}
	}
	break;
	case eContainerType::CUSTOM_DIY_MAP:
		person = (*customMap[uniqueID]);
		if ((*customMap[uniqueID]).first != "")
			return true;
		break;
	}
	endCount();
	return false;
}

bool cLukesProjectClass::FindPeople(std::vector<sPerson>& vecPeople, sPoint location, float radius, int maxPeopleToReturn)
{

	startTime = std::chrono::steady_clock::now();
	start_memUsage = getCurrentMemUse();

	switch (currentContainerType)
	{
	case eContainerType::STD_VECTOR:
		for (int i = 0; i < peopleVector.size(); ++i)
		{
			if (Distance(peopleVector[i].location, location) <= radius)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back(peopleVector[i]);
			}
		}
		break;
	case eContainerType::STD_LIST:
		for (std::list<sPerson>::iterator iterator = peopleList.begin(), end = peopleList.end(); iterator != end; ++iterator) {

			if (Distance((*iterator).location, location) <= radius)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back((*iterator));
			}
		}
		break;
	case eContainerType::STD_MAP:
		for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {

			if (Distance((*iterator).second.location, location) <= radius)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back((*iterator).second);
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_VECTOR:
		for (int i = 0; i < customVector.Size(); ++i)
		{
			if (Distance(customVector[i].location, location) <= radius)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back(customVector[i]);
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_LIST:
	{
		cListItem* current = customList.first;
		while (current != 0)
		{
			if (Distance(current->value.location, location) <= radius)
			{
				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back(current->value);
			}
			current = current->nextValue;
		}
	}
	break;
	case eContainerType::CUSTOM_DIY_MAP:
		for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
		{
			for (int k = 0; k < customMap.people[i].Size(); ++k)
			{
				if (Distance(customMap.people[i][k].location, location) <= radius)
				{
					if (vecPeople.size() < maxPeopleToReturn)
						vecPeople.push_back(customVector[k]);
				}
			}
		}
		break;
	}
	endCount();
	return true;
}

bool cLukesProjectClass::FindPeople(std::vector<sPerson>& vecPeople, float minHealth, float maxHealth, int maxPeopleToReturn)
{

	startTime = std::chrono::steady_clock::now();
	start_memUsage = getCurrentMemUse();

	switch (currentContainerType)
	{
	case eContainerType::STD_VECTOR:
		for (int i = 0; i < peopleVector.size(); ++i)
		{
			if (peopleVector[i].health >= minHealth && peopleVector[i].health <= maxHealth)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back(peopleVector[i]);
			}
		}
		break;
	case eContainerType::STD_LIST:
		for (std::list<sPerson>::iterator iterator = peopleList.begin(), end = peopleList.end(); iterator != end; ++iterator) {

			if ((*iterator).health >= minHealth && (*iterator).health <= maxHealth)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back((*iterator));
			}
		}
		break;
	case eContainerType::STD_MAP:
		for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {

			if ((*iterator).second.health >= minHealth && (*iterator).second.health <= maxHealth)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back((*iterator).second);
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_VECTOR:
		for (int i = 0; i < customVector.Size(); ++i)
		{
			if (customVector[i].health >= minHealth && customVector[i].health <= maxHealth)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back(customVector[i]);
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_LIST:
	{
		cListItem* current = customList.first;
		while (current != 0)
		{
			if (current->value.health >= minHealth && current->value.health <= maxHealth)
			{
				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back(current->value);
			}
			current = current->nextValue;
		}
	}
	break;
	case eContainerType::CUSTOM_DIY_MAP:
		for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
		{
			for (int k = 0; k < customMap.people[i].Size(); ++k)
			{
				if (customMap.people[i][k].health >= minHealth && customMap.people[i][k].health <= maxHealth)
				{
					if (vecPeople.size() < maxPeopleToReturn)
						vecPeople.push_back(customVector[k]);
				}
			}
		}
		break;
	}
	endCount();
	return true;
}

bool cLukesProjectClass::FindPeople(std::vector<sPerson>& vecPeople, sPoint location, float radius, float minHealth, float maxHealth, int maxPeopleToReturn)
{

	startTime = std::chrono::steady_clock::now();
	start_memUsage = getCurrentMemUse();

	switch (currentContainerType)
	{
	case eContainerType::STD_VECTOR:
		for (int i = 0; i < peopleVector.size(); ++i)
		{
			if (peopleVector[i].health >= minHealth && peopleVector[i].health <= maxHealth && Distance(peopleVector[i].location, location) <= radius)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back(peopleVector[i]);
			}
		}
		break;
	case eContainerType::STD_LIST:
		for (std::list<sPerson>::iterator iterator = peopleList.begin(), end = peopleList.end(); iterator != end; ++iterator) {

			if ((*iterator).health >= minHealth && (*iterator).health <= maxHealth && Distance(location, (*iterator).location) <= radius)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back((*iterator));
			}
		}
		break;
	case eContainerType::STD_MAP:
		for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {

			if ((*iterator).second.health >= minHealth && (*iterator).second.health <= maxHealth && Distance(location, (*iterator).second.location) <= radius)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back((*iterator).second);
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_VECTOR:
		for (int i = 0; i < customVector.Size(); ++i)
		{
			if (customVector[i].health >= minHealth && customVector[i].health <= maxHealth && Distance(customVector[i].location, location) <= radius)
			{

				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back(customVector[i]);
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_LIST:
	{
		cListItem* current = customList.first;
		while (current != 0)
		{
			if (current->value.health >= minHealth && current->value.health <= maxHealth && Distance(current->value.location, location) <= radius)
			{
				if (vecPeople.size() < maxPeopleToReturn)
					vecPeople.push_back(current->value);
			}
			current = current->nextValue;
		}
	}
	break;
	case eContainerType::CUSTOM_DIY_MAP:
		for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
		{

			for (int k = 0; k < customMap.people[i].Size(); ++k)
			{
				if (customMap.people[i][k].health >= minHealth && customMap.people[i][k].health <= maxHealth && Distance(customMap.people[i][k].location, location) <= radius)
				{
					if (vecPeople.size() < maxPeopleToReturn)
						vecPeople.push_back(customVector[k]);
				}
			}
		}
		break;
	}
	endCount();
	return true;
}

bool cLukesProjectClass::SortPeople(std::vector<sPerson>& vecPeople, eSortType sortType)
{

	startTime = std::chrono::steady_clock::now();
	start_memUsage = getCurrentMemUse();

	switch (currentContainerType)
	{
	case eContainerType::STD_VECTOR:
		switch (sortType)
		{
		case iPersonMotron::ASC_FIRST_THEN_LAST:
			std::sort(peopleVector.begin(), peopleVector.end(), CompareASCFIRSTTHENLAST);
			break;
		case iPersonMotron::DESC_FIRST_THEN_LAST:
			std::sort(peopleVector.begin(), peopleVector.end(), CompareDESCFIRSTTHENLAST);
			break;
		case iPersonMotron::ASC_LAST_THEN_FIRST:
			std::sort(peopleVector.begin(), peopleVector.end(), CompareASCLASTTHENFIRST);
			break;
		case iPersonMotron::DESC_LAST_THEN_FIRST:
			std::sort(peopleVector.begin(), peopleVector.end(), CompareDESCLASTTHENFIRST);
			break;
		case iPersonMotron::ASC_BY_ID:
			std::sort(peopleVector.begin(), peopleVector.end(), CompareASCBYID);
			break;
		case iPersonMotron::DESC_BY_ID:
			std::sort(peopleVector.begin(), peopleVector.end(), CompareDESCBYID);
			break;
		case iPersonMotron::ASC_BY_HEALTH:
			std::sort(peopleVector.begin(), peopleVector.end(), CompareASCBYHEALTH);
			break;
		case iPersonMotron::DESC_BY_HEALTH:
			std::sort(peopleVector.begin(), peopleVector.end(), CompareDESCBYHEALTH);
			break;
		}
		//then copy the information to the return vector
		vecPeople = peopleVector;
		break;
	case eContainerType::STD_LIST:
		switch (sortType)
		{
		case iPersonMotron::ASC_FIRST_THEN_LAST:
			peopleList.sort(CompareASCFIRSTTHENLAST);
			break;
		case iPersonMotron::DESC_FIRST_THEN_LAST:
			peopleList.sort(CompareDESCFIRSTTHENLAST);
			break;
		case iPersonMotron::ASC_LAST_THEN_FIRST:
			peopleList.sort(CompareASCLASTTHENFIRST);
			break;
		case iPersonMotron::DESC_LAST_THEN_FIRST:
			peopleList.sort(CompareDESCLASTTHENFIRST);
			break;
		case iPersonMotron::ASC_BY_ID:
			peopleList.sort(CompareASCBYID);
			break;
		case iPersonMotron::DESC_BY_ID:
			peopleList.sort(CompareDESCBYID);
			break;
		case iPersonMotron::ASC_BY_HEALTH:
			peopleList.sort(CompareASCBYHEALTH);
			break;
		case iPersonMotron::DESC_BY_HEALTH:
			peopleList.sort(CompareDESCBYHEALTH);
			break;
		}

		//then copy the information to the return vector
		for (std::list<sPerson>::iterator iterator = peopleList.begin(), end = peopleList.end(); iterator != end; ++iterator) {

			vecPeople.push_back((*iterator));
		}
		break;
	case eContainerType::STD_MAP:
		switch (sortType)
		{
		case iPersonMotron::ASC_FIRST_THEN_LAST:
		{
			std::map<std::string, std::map<int, sPerson>> sortedMap;

			for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {
				//if a map already exists there, simply add our person into that map
				if (sortedMap.find((*iterator).second.first + (*iterator).second.last) != sortedMap.end())
				{
					sortedMap[(*iterator).second.first + (*iterator).second.last][(*iterator).second.uniqueID] = (*iterator).second;
				}
				else //otherwise create a new map
				{
					std::map<int, sPerson> tempMap;
					tempMap[(*iterator).second.uniqueID] = (*iterator).second;
					sortedMap[(*iterator).second.first + (*iterator).second.last] = tempMap;
				}
			}

			for (std::map<std::string, std::map<int, sPerson>>::iterator iterator = sortedMap.begin(), end = sortedMap.end(); iterator != end; ++iterator)
			{
				for (std::map<int, sPerson>::iterator secondaryIterator = (*iterator).second.begin(), end = (*iterator).second.end(); secondaryIterator != end; ++secondaryIterator)
				{
					vecPeople.push_back((*secondaryIterator).second);
				}
			}
		}
		break;
		case iPersonMotron::DESC_FIRST_THEN_LAST:
		{

			std::map<std::string, std::map<int, sPerson>> sortedMap;

			for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {
				//if a map already exists there, simply add our person into that map
				if (sortedMap.find((*iterator).second.first + (*iterator).second.last) != sortedMap.end())
				{
					sortedMap[(*iterator).second.first + (*iterator).second.last][(*iterator).second.uniqueID] = (*iterator).second;
				}
				else //otherwise create a new map
				{
					std::map<int, sPerson> tempMap;
					tempMap[(*iterator).second.uniqueID] = (*iterator).second;
					sortedMap[(*iterator).second.first + (*iterator).second.last] = tempMap;
				}
			}

			for (std::map<std::string, std::map<int, sPerson>>::reverse_iterator iterator = sortedMap.rbegin(), end = sortedMap.rend(); iterator != end; ++iterator)
			{
				for (std::map<int, sPerson>::reverse_iterator secondaryIterator = (*iterator).second.rbegin(), end = (*iterator).second.rend(); secondaryIterator != end; ++secondaryIterator)
				{
					vecPeople.push_back((*secondaryIterator).second);
				}
			}
		}
		break;
		case iPersonMotron::ASC_LAST_THEN_FIRST:
		{

			std::map<std::string, std::map<int, sPerson>> sortedMap;

			for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {
				//if a map already exists there, simply add our person into that map
				if (sortedMap.find((*iterator).second.last + (*iterator).second.first) != sortedMap.end())
				{
					sortedMap[(*iterator).second.last + (*iterator).second.first][(*iterator).second.uniqueID] = (*iterator).second;
				}
				else //otherwise create a new map
				{
					std::map<int, sPerson> tempMap;
					tempMap[(*iterator).second.uniqueID] = (*iterator).second;
					sortedMap[(*iterator).second.last + (*iterator).second.first] = tempMap;
				}
			}

			for (std::map<std::string, std::map<int, sPerson>>::iterator iterator = sortedMap.begin(), end = sortedMap.end(); iterator != end; ++iterator)
			{
				for (std::map<int, sPerson>::iterator secondaryIterator = (*iterator).second.begin(), end = (*iterator).second.end(); secondaryIterator != end; ++secondaryIterator)
				{
					vecPeople.push_back((*secondaryIterator).second);
				}
			}
		}
		break;
		case iPersonMotron::DESC_LAST_THEN_FIRST:
		{
			std::map<std::string, std::map<int, sPerson>> sortedMap;

			for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {
				//if a map already exists there, simply add our person into that map
				if (sortedMap.find((*iterator).second.last + (*iterator).second.first) != sortedMap.end())
				{
					sortedMap[(*iterator).second.last + (*iterator).second.first][(*iterator).second.uniqueID] = (*iterator).second;
				}
				else //otherwise create a new map
				{
					std::map<int, sPerson> tempMap;
					tempMap[(*iterator).second.uniqueID] = (*iterator).second;
					sortedMap[(*iterator).second.last + (*iterator).second.first] = tempMap;
				}
			}

			for (std::map<std::string, std::map<int, sPerson>>::reverse_iterator iterator = sortedMap.rbegin(), end = sortedMap.rend(); iterator != end; ++iterator)
			{
				for (std::map<int, sPerson>::reverse_iterator secondaryIterator = (*iterator).second.rbegin(), end = (*iterator).second.rend(); secondaryIterator != end; ++secondaryIterator)
				{
					vecPeople.push_back((*secondaryIterator).second);
				}
			}

		}
		break;
		case iPersonMotron::ASC_BY_ID:
			for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator)
			{
				vecPeople.push_back((*iterator).second);
			}
			break;
		case iPersonMotron::DESC_BY_ID:
			for (std::map<int, sPerson>::reverse_iterator iterator = peopleMap.rbegin(), end = peopleMap.rend(); iterator != end; ++iterator)
			{
				vecPeople.push_back((*iterator).second);
			}
			break;
		case iPersonMotron::ASC_BY_HEALTH:
		{
			std::map<float, std::map<int, sPerson>> sortedMap;

			for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {
				//if a map already exists there, simply add our person into that map
				if (sortedMap.find((*iterator).second.health) != sortedMap.end())
				{
					sortedMap[(*iterator).second.health][(*iterator).second.uniqueID] = (*iterator).second;
				}
				else //otherwise create a new map
				{
					std::map<int, sPerson> tempMap;
					tempMap[(*iterator).second.uniqueID] = (*iterator).second;
					sortedMap[(*iterator).second.health] = tempMap;
				}
			}

			for (std::map<float, std::map<int, sPerson>>::iterator iterator = sortedMap.begin(), end = sortedMap.end(); iterator != end; ++iterator)
			{
				for (std::map<int, sPerson>::iterator secondaryIterator = (*iterator).second.begin(), end = (*iterator).second.end(); secondaryIterator != end; ++secondaryIterator)
				{
					vecPeople.push_back((*secondaryIterator).second);
				}
			}
		}
		break;
		case iPersonMotron::DESC_BY_HEALTH:
		{
			std::map<float, std::map<int, sPerson>> sortedMap;

			for (std::map<int, sPerson>::iterator iterator = peopleMap.begin(), end = peopleMap.end(); iterator != end; ++iterator) {
				//if a map already exists there, simply add our person into that map
				if (sortedMap.find((*iterator).second.health) != sortedMap.end())
				{
					sortedMap[(*iterator).second.health][(*iterator).second.uniqueID] = (*iterator).second;
				}
				else //otherwise create a new map
				{
					std::map<int, sPerson> tempMap;
					tempMap[(*iterator).second.uniqueID] = (*iterator).second;
					sortedMap[(*iterator).second.health] = tempMap;
				}
			}

			for (std::map<float, std::map<int, sPerson>>::reverse_iterator iterator = sortedMap.rbegin(), end = sortedMap.rend(); iterator != end; ++iterator)
			{
				for (std::map<int, sPerson>::reverse_iterator secondaryIterator = (*iterator).second.rbegin(), end = (*iterator).second.rend(); secondaryIterator != end; ++secondaryIterator)
				{
					vecPeople.push_back((*secondaryIterator).second);
				}
			}
		}
		break;
		}
		break;
	case eContainerType::CUSTOM_DIY_VECTOR:
		switch (sortType)
		{
		case iPersonMotron::ASC_FIRST_THEN_LAST:
			customVector.Sort(0, customVector.Size() - 1, CompareASCFIRSTTHENLAST);
			break;
		case iPersonMotron::DESC_FIRST_THEN_LAST:
			customVector.Sort(0, customVector.Size() - 1, CompareDESCFIRSTTHENLAST);
			break;
		case iPersonMotron::ASC_LAST_THEN_FIRST:
			customVector.Sort(0, customVector.Size() - 1, CompareASCLASTTHENFIRST);
			break;
		case iPersonMotron::DESC_LAST_THEN_FIRST:
			customVector.Sort(0, customVector.Size() - 1, CompareDESCLASTTHENFIRST);
			break;
		case iPersonMotron::ASC_BY_ID:
			customVector.Sort(0, customVector.Size() - 1, CompareASCBYID);
			break;
		case iPersonMotron::DESC_BY_ID:
			customVector.Sort(0, customVector.Size() - 1, CompareDESCBYID);
			break;
		case iPersonMotron::ASC_BY_HEALTH:
			customVector.Sort(0, customVector.Size() - 1, CompareASCBYHEALTH);
			break;
		case iPersonMotron::DESC_BY_HEALTH:
			customVector.Sort(0, customVector.Size() - 1, CompareDESCBYHEALTH);
			break;
		}

		for (int i = 0; i < customVector.Size(); ++i)
		{
			vecPeople.push_back(customVector[i]);
		}
		break;
	case eContainerType::CUSTOM_DIY_LIST:
		switch (sortType)
		{
		case iPersonMotron::ASC_FIRST_THEN_LAST:
			customList.Sort(CompareASCFIRSTTHENLAST);
			break;
		case iPersonMotron::DESC_FIRST_THEN_LAST:
			customList.Sort(CompareDESCFIRSTTHENLAST);
			break;
		case iPersonMotron::ASC_LAST_THEN_FIRST:
			customList.Sort(CompareASCLASTTHENFIRST);
			break;
		case iPersonMotron::DESC_LAST_THEN_FIRST:
			customList.Sort(CompareDESCLASTTHENFIRST);
			break;
		case iPersonMotron::ASC_BY_ID:
			customList.Sort(CompareASCBYID);
			break;
		case iPersonMotron::DESC_BY_ID:
			customList.Sort(CompareDESCBYID);
			break;
		case iPersonMotron::ASC_BY_HEALTH:
			customList.Sort(CompareASCBYHEALTH);
			break;
		case iPersonMotron::DESC_BY_HEALTH:
			customList.Sort(CompareDESCBYHEALTH);
			break;
		}
		{
			cListItem* current = customList.first;
			while (current != 0)
			{
				vecPeople.push_back(current->value);
				current = current->nextValue;
			}
		}
		break;
	case eContainerType::CUSTOM_DIY_MAP:
		switch (sortType)
		{
		case iPersonMotron::ASC_FIRST_THEN_LAST:
		{
			cMap sortedMap;
			for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < customMap.people[i].Size(); ++k)
				{
					sortedMap.Insert(customMap.people[i][k].first.substr(0,2), customMap.people[i][k]);
				}
			}

			for (int i = 0; i < sortedMap.INITALARRAYSIZE; ++i)
			{
				//if there is a vector, sort it
				if (sortedMap.people[i].Size() > 0)
				{
					sortedMap.people[i].Sort(0, sortedMap.people[i].Size() - 1, CompareASCFIRSTTHENLAST);
				}
			}

			for (int i = 0; i < sortedMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < sortedMap.people[i].Size(); ++k)
				{
					vecPeople.push_back(sortedMap.people[i][k]);
				}
			}



		}
		break;
		case iPersonMotron::DESC_FIRST_THEN_LAST:
		{

			cMap sortedMap;
			for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < customMap.people[i].Size(); ++k)
				{
					sortedMap.Insert(customMap.people[i][k].first.substr(0, 2), customMap.people[i][k]);
				}
			}

			for (int i = 0; i < sortedMap.INITALARRAYSIZE; ++i)
			{
				//if there is a vector, sort it
				if (sortedMap.people[i].Size() > 0)
				{
					sortedMap.people[i].Sort(0, sortedMap.people[i].Size() - 1, CompareDESCFIRSTTHENLAST);
				}
			}

			for (int i = sortedMap.INITALARRAYSIZE; i >= 0; --i)
			{
				for (int k = 0; k < sortedMap.people[i].Size(); ++k)
				{
					vecPeople.push_back(sortedMap.people[i][k]);
				}
			}
		}
		break;
		case iPersonMotron::ASC_LAST_THEN_FIRST:
		{
			cMap sortedMap;
			for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < customMap.people[i].Size(); ++k)
				{
					sortedMap.Insert(customMap.people[i][k].last.substr(0, 2), customMap.people[i][k]);
				}
			}

			for (int i = 0; i < sortedMap.INITALARRAYSIZE; ++i)
			{
				//if there is a vector, sort it
				if (sortedMap.people[i].Size() > 0)
				{
					sortedMap.people[i].Sort(0, sortedMap.people[i].Size() - 1, CompareASCLASTTHENFIRST);
				}
			}

			for (int i = 0; i < sortedMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < sortedMap.people[i].Size(); ++k)
				{
					vecPeople.push_back(sortedMap.people[i][k]);
				}
			}




		}
		break;
		case iPersonMotron::DESC_LAST_THEN_FIRST:
		{
			cMap sortedMap;
			for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < customMap.people[i].Size(); ++k)
				{
					sortedMap.Insert(customMap.people[i][k].last.substr(0, 2), customMap.people[i][k]);
				}
			}

			for (int i = 0; i < sortedMap.INITALARRAYSIZE; ++i)
			{
				//if there is a vector, sort it
				if (sortedMap.people[i].Size() > 0)
				{
					sortedMap.people[i].Sort(0, sortedMap.people[i].Size() - 1, CompareDESCLASTTHENFIRST);
				}
			}

			for (int i = sortedMap.INITALARRAYSIZE; i >= 0; --i)
			{
				for (int k = 0; k < sortedMap.people[i].Size(); ++k)
				{
					vecPeople.push_back(sortedMap.people[i][k]);
				}
			}
		}
		break;
		case iPersonMotron::ASC_BY_ID:
		{
			for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < customMap.people[i].Size(); ++k)
				{
					vecPeople.push_back(customMap.people[i][k]);
				}
			}
		}
		break;
		case iPersonMotron::DESC_BY_ID:
		{
			for (int i = customMap.INITALARRAYSIZE - 1; i >= 0; --i)
			{
				for (int k = 0; k < customMap.people[i].Size(); ++k)
				{
					vecPeople.push_back(customMap.people[i][k]);
				}
			}
		}
		break;
		case iPersonMotron::ASC_BY_HEALTH:
		{
			cMap sortedMap;
			for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < customMap.people[i].Size(); ++k)
				{
					sortedMap.Insert(customMap.people[i][k].health, customMap.people[i][k]);
				}
			}

			for (int i = 0; i < sortedMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < sortedMap.people[i].Size(); ++k)
				{
					vecPeople.push_back(sortedMap.people[i][k]);
				}
			}



		}
		break;
		case iPersonMotron::DESC_BY_HEALTH:
		{
			cMap sortedMap;
			for (int i = 0; i < customMap.INITALARRAYSIZE; ++i)
			{
				for (int k = 0; k < customMap.people[i].Size(); ++k)
				{
					sortedMap.Insert(customMap.people[i][k].health, customMap.people[i][k]);
				}
			}

			for (int i = sortedMap.INITALARRAYSIZE - 1; i >= 0; --i)
			{
				for (int k = 0; k < sortedMap.people[i].Size(); ++k)
				{
					vecPeople.push_back(sortedMap.people[i][k]);
				}
			}
		}
		break;
		}
		break;
	}
	endCount();

	if (vecPeople.size() == 0)
		return false;
	return true;
}

bool cLukesProjectClass::GetPerformanceFromLastCall(sPerfData & callStats)
{
	std::cout << "\n\n=========================================" << std::endl;
	std::cout << "   Elapsed Time in MS: " << callStats.elapsedCallTime_ms << std::endl;
	std::cout << "   Memory Usage in Bytes [MIN]: " << std::setprecision(9) << callStats.memoryUsageBytes_min << std::endl;
	std::cout << "   Memory Usage in Bytes [MAX]: " << std::setprecision(9) << callStats.memoryUsageBytes_max << std::endl;
	std::cout << "   Memory Usage in Bytes [AVG]: " << std::setprecision(9) << callStats.memoryUsageBytes_avg << std::endl;
	std::cout << "=========================================\n" << std::endl;

	return true;
}

eContainerType cLukesProjectClass::getContainerType(void)
{
	return currentContainerType;
}

void cLukesProjectClass::setContainerType(eContainerType type)
{

	this->currentContainerType = type;
	switch (type)
	{
	case CUSTOM_DIY_VECTOR:
		cVecMaleFirstNames = new cVector();
		cVecFemaleFirstNames = new cVector();
		cVecLastNames = new cVector();
		break;
	case CUSTOM_DIY_LIST:
		cListMaleFirstNames = new cList();
		cListFemaleFirstNames = new cList();
		cListLastNames = new cList();
		break;
	case CUSTOM_DIY_MAP:
		cMapMaleFirstNames = new cMap();
		cMapFemaleFirstNames = new cMap();
		cMapLastNames = new cMap();
		break;
	}
}

void cLukesProjectClass::addPerson(sPerson personToAdd)
{
	switch (currentContainerType)
	{
	case eContainerType::STD_VECTOR:
		peopleVector.push_back(personToAdd);
		break;
	case eContainerType::STD_LIST:
		peopleList.insert(peopleList.begin(), personToAdd);
		break;
	case eContainerType::STD_MAP:
		peopleMap[personToAdd.uniqueID] = personToAdd;
		break;
	case eContainerType::CUSTOM_DIY_VECTOR:
		customVector.PushBack(personToAdd);
		break;
	case eContainerType::CUSTOM_DIY_LIST:
		customList.InsertAt(0, personToAdd);
		break;
	case eContainerType::CUSTOM_DIY_MAP:
		(*customMap[personToAdd.uniqueID]) = personToAdd;
		break;
	}
}

float cLukesProjectClass::getCurrentMemUse()
{
	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
	SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;
	SIZE_T physMemUsedByMe = pmc.WorkingSetSize;

	return (float)physMemUsedByMe;
}

void cLukesProjectClass::init()
{
	SYSTEM_INFO sysInfo;
	FILETIME ftime, fsys, fuser;

	GetSystemInfo(&sysInfo);
	numProcessors = sysInfo.dwNumberOfProcessors;

	GetSystemTimeAsFileTime(&ftime);
	memcpy(&lastCPU, &ftime, sizeof(FILETIME));

	self = GetCurrentProcess();
	GetProcessTimes(self, &ftime, &ftime, &fsys, &fuser);
	memcpy(&lastSysCPU, &fsys, sizeof(FILETIME));
	memcpy(&lastUserCPU, &fuser, sizeof(FILETIME));
}
